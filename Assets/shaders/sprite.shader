// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:0,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1873,x:33683,y:32679,varname:node_1873,prsc:2|emission-8701-OUT,alpha-603-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32551,y:32835,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,tex:0535dca7b09f0ad4581a2bb352965f3a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32551,y:33004,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:603,x:32956,y:32998,cmnt:A,varname:node_603,prsc:2|A-4805-A,B-5376-A;n:type:ShaderForge.SFN_Color,id:2922,x:32543,y:32656,ptovrint:False,ptlb:swapColor,ptin:_swapColor,varname:node_2922,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.4758621,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3737,x:33013,y:32545,ptovrint:False,ptlb:colorMask,ptin:_colorMask,varname:node_3737,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7ae5c95b0c9600341aa565d56975941d,ntxv:0,isnm:False;n:type:ShaderForge.SFN_If,id:9225,x:33013,y:32770,varname:node_9225,prsc:2|A-3737-A,B-843-OUT,GT-299-OUT,EQ-299-OUT,LT-4805-RGB;n:type:ShaderForge.SFN_Vector1,id:843,x:33013,y:32700,varname:node_843,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:8701,x:33368,y:32770,varname:node_8701,prsc:2|A-9356-OUT,B-603-OUT;n:type:ShaderForge.SFN_Tex2d,id:8401,x:32543,y:32453,ptovrint:False,ptlb:head,ptin:_head,varname:node_8401,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:4630d87b65cc2e24d9ffcc5b69c37105,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:299,x:32779,y:32666,varname:node_299,prsc:2|A-2922-RGB,B-4805-RGB;n:type:ShaderForge.SFN_Tex2d,id:674,x:33313,y:32198,ptovrint:False,ptlb:headMask,ptin:_headMask,varname:node_674,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1dd6762feeea0634292cba9ea4372bf5,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Vector1,id:2888,x:33313,y:32353,varname:node_2888,prsc:2,v1:1;n:type:ShaderForge.SFN_If,id:9356,x:33313,y:32417,varname:node_9356,prsc:2|A-674-A,B-2888-OUT,GT-5222-OUT,EQ-5222-OUT,LT-9225-OUT;n:type:ShaderForge.SFN_Multiply,id:5222,x:32730,y:32453,varname:node_5222,prsc:2|A-8401-RGB,B-4805-RGB;proporder:4805-2922-3737-8401-674;pass:END;sub:END;*/

Shader "Shader Forge/sprite" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        _swapColor ("swapColor", Color) = (1,0.4758621,0,1)
        _colorMask ("colorMask", 2D) = "white" {}
        _head ("head", 2D) = "white" {}
        _headMask ("headMask", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _swapColor;
            uniform sampler2D _colorMask; uniform float4 _colorMask_ST;
            uniform sampler2D _head; uniform float4 _head_ST;
            uniform sampler2D _headMask; uniform float4 _headMask_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _headMask_var = tex2D(_headMask,TRANSFORM_TEX(i.uv0, _headMask));
                float node_9356_if_leA = step(_headMask_var.a,1.0);
                float node_9356_if_leB = step(1.0,_headMask_var.a);
                float4 _colorMask_var = tex2D(_colorMask,TRANSFORM_TEX(i.uv0, _colorMask));
                float node_9225_if_leA = step(_colorMask_var.a,1.0);
                float node_9225_if_leB = step(1.0,_colorMask_var.a);
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_299 = (_swapColor.rgb*_MainTex_var.rgb);
                float4 _head_var = tex2D(_head,TRANSFORM_TEX(i.uv0, _head));
                float3 node_5222 = (_head_var.rgb*_MainTex_var.rgb);
                float node_603 = (_MainTex_var.a*i.vertexColor.a); // A
                float3 emissive = (lerp((node_9356_if_leA*lerp((node_9225_if_leA*_MainTex_var.rgb)+(node_9225_if_leB*node_299),node_299,node_9225_if_leA*node_9225_if_leB))+(node_9356_if_leB*node_5222),node_5222,node_9356_if_leA*node_9356_if_leB)*node_603);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_603);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
