﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public GameObject playerPrefab;
    private LevelController level;

    void Start()
    {
        level = GameObject.FindGameObjectWithTag("LevelController").GetComponent<LevelController>();
        LoadLevel();
        LoadPlayerPrefabs();
    }

    private void LoadLevel()
    {
        level.LoadLevel();
    }

    private void LoadPlayerPrefabs()
    {
        for (int i = 0; i < 4; i++)
        {
            GameObject player = Instantiate(playerPrefab, level.PlayerSpawns[i], Quaternion.identity) as GameObject;
            player.GetComponent<PlayerController>().Setup(i);
        }
    }
}
