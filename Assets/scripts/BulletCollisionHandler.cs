﻿using UnityEngine;
using System.Collections;

public class BulletCollisionHandler : MonoBehaviour
{
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Tile")
        {
            // hit a wall
            Destroy(gameObject);
        }
    }
}
