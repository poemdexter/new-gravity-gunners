﻿using UnityEngine;
using System.Collections;
using Rewired;

public class PlayerController : MonoBehaviour
{
    public float spinPower = 500.0f;
    public Vector2 weaponOffset = new Vector2(.46f, .06f);

    public bool IsDead { get; private set; }

    private int playerId;
    private bool isPlaying = false;
    private Weapon weapon;
    private Player player;
    private SpriteRenderer spriteRenderer;
    private CircleCollider2D circleCollider;

    void Awake() {}

    void Start()
    {
        weapon = GetComponent<Weapon>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        circleCollider = GetComponent<CircleCollider2D>();
    }

    void Update()
    {
        if (isPlaying)
        {
            if (!IsDead)
            {
                transform.Rotate(Vector3.back, spinPower * player.GetAxis("Rotate") * Time.deltaTime, Space.World);

                if (player.GetButtonDown("Shoot"))
                {
                    weapon.Fire(transform.position + (transform.rotation * weaponOffset), transform.rotation);
                }
            }
        }
        else
        {
            if (player.GetButtonDown("Shoot"))
            {
                ActivatePlayer();
            }
        }
    }

    public void Setup(int id)
    {
        playerId = id;
        tag = (id + 1).ToString();
        player = ReInput.players.GetPlayer(playerId);
        GetComponent<SpriteController>().Setup(id);
    }

    private void ActivatePlayer()
    {
        spriteRenderer.enabled = true;
        circleCollider.enabled = true;
        isPlaying = true;
    }

    public void Die()
    {
        spriteRenderer.enabled = false;
        circleCollider.enabled = false;
        IsDead = true;
    }
}
