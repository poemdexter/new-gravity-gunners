﻿using UnityEngine;
using System.Collections;

public class SpriteController : MonoBehaviour
{
    public Texture2D[] suitMasks;
    public Sprite[] suits;
    public Texture2D[] heads;

    private Color[] colors = new Color[] { Color.green, Color.red, Color.yellow, Color.blue };

    public void Setup(int i)
    {
        ChangeColor(i);
        ChangeSuit(i);
        ChangeHead(i);
    }

    void ChangeColor(int i)
    {
        
        GetComponent<Renderer>().material.SetColor("_swapColor", colors[i]);
    }

    void ChangeSuit(int i)
    {
        GetComponent<SpriteRenderer>().sprite = suits[i];
        GetComponent<Renderer>().material.SetTexture("_colorMask", suitMasks[i]);
    }

    void ChangeHead(int i)
    {
        GetComponent<Renderer>().material.SetTexture("_head", heads[i]);
    }
}
