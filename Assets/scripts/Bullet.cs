﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    public float speed;

    public string Owner { get; private set; }

    void Awake() { }

    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime, Space.Self);
    }

    public void SetOwner(string playerTag)
    {
        Owner = playerTag;
    }
}
