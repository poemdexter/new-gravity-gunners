﻿using UnityEngine;
using System.Collections;
using TinyJSON;

public class LevelController : MonoBehaviour
{
    public TextAsset levelData;
    public GameObject colliderPrefab;
    public float tileSize = 0.625f;
    public Vector2[] PlayerSpawns { get; private set; }

    void Awake() 
    {
        PlayerSpawns = new Vector2[4];
    }

    void Start() { }

    public void LoadLevel()
    {
        var data = JSON.Load(levelData.text);
        foreach (var layer in data["layers"] as ProxyArray)
        {
            if ((string)layer["name"] == "Colliders")
            {
                foreach (var collider in layer["objects"] as ProxyArray)
                {
                    int x = collider["x"] / 20;
                    int y = collider["y"] / 20;
                    int w = collider["width"] / 20;
                    int h = collider["height"] / 20;
                    CreateCollider(x, y, w, h);
                }
            } 
            else if ((string)layer["name"] == "Spawns")
            {
                foreach (var spawn in layer["objects"] as ProxyArray)
                {
                    string id = "-1";
                    foreach (var pair in spawn["properties"] as ProxyObject) 
                    {
                        if (pair.Key == "id") { id = pair.Value; }
                    }
                    PlayerSpawns[int.Parse(id) - 1] = new Vector2(calculatePositionX(spawn["x"] / 20, 1), calculatePositionY(spawn["y"] / 20, 1));
                }
            }
        }
    }

    private void CreateCollider(int x, int y, int width, int height)
    {
        float xPos = calculatePositionX(x, width);
        float yPos = calculatePositionY(y, height);
        GameObject collider = (GameObject)Instantiate(colliderPrefab, new Vector3(xPos, yPos, 0f), Quaternion.identity);
        collider.transform.parent = this.transform;
        BoxCollider2D box = collider.GetComponent<BoxCollider2D>();
        box.size = new Vector2(tileSize * width, tileSize * height);
    }

    private float calculatePositionX(int x, int width)
    {
        float bottomLeftStart = -20 * tileSize;
        float colliderCenterPosition = x * tileSize + (width / 2) * tileSize;
        float evenOrOddWidthOffset = (width % 2) * (tileSize / 2);

        return bottomLeftStart + colliderCenterPosition + evenOrOddWidthOffset;
    }

    // tiled origin is top left and not bottom left which is why this calculation is slightly different.
    private float calculatePositionY(int y, int height)
    {
        float bottomLeftStart = 11 * tileSize;
        float colliderCenterPosition = y * tileSize + (height / 2) * tileSize;
        float evenOrOddWidthOffset = ((height - 1) % 2) * (tileSize / 2);

        return bottomLeftStart - colliderCenterPosition + evenOrOddWidthOffset;
    }
}
