﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float kickbackPower;
    public float maxVelocity = 10f;

    private Rigidbody2D body;
    
    void Awake() { }

    void Start() 
    {
        body = GetComponent<Rigidbody2D>();
    }

    public void Fire(Vector2 position, Quaternion rotation)
    {
        Bullet bullet = (Instantiate(bulletPrefab, position, rotation) as GameObject).GetComponent<Bullet>();
        bullet.gameObject.tag = "Bullet" + gameObject.tag;

        body.AddForce(-transform.right * kickbackPower);
        
        if (body.velocity.magnitude >= maxVelocity)
        {
            body.velocity = body.velocity.normalized * maxVelocity;
        }
    }
}
