﻿using UnityEngine;
using System.Collections;

public class ScrollingBackground : MonoBehaviour
{
    public GameObject backgroundPrefab;
    public float scrollSpeed;
    public float xOffset;

    private GameObject[] backgrounds = new GameObject[3];

    void Start()
    {
        backgrounds[0] = GameObject.Instantiate(backgroundPrefab, Vector2.zero, Quaternion.identity) as GameObject;
        backgrounds[1] = GameObject.Instantiate(backgroundPrefab, new Vector2(xOffset, 0), Quaternion.identity) as GameObject;

        backgrounds[0].transform.parent = transform;
        backgrounds[1].transform.parent = transform;
    }

    void Update()
    {
        // move rightmost tile first to avoid crease
        backgrounds[0].transform.Translate(Vector2.left * scrollSpeed * Time.deltaTime);
        backgrounds[1].transform.Translate(Vector2.left * scrollSpeed * Time.deltaTime);
            
        if (backgrounds[0].transform.position.x < -xOffset) // moved too far left and need to reset
        {
            backgrounds[0].transform.position = Vector2.zero;
            backgrounds[1].transform.position = new Vector2(xOffset, 0);
        }
    }
}
