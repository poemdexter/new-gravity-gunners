﻿using UnityEngine;
using System.Collections;

public class PlayerCollisionHandler : MonoBehaviour
{
    public bool IsDead { get; private set; }

    void Awake()
    {
        IsDead = false;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet1") 
            || collision.gameObject.CompareTag("Bullet2") 
            || collision.gameObject.CompareTag("Bullet3") 
            || collision.gameObject.CompareTag("Bullet4"))
        {
            if (!collision.gameObject.CompareTag("Bullet" + gameObject.tag)) // we're shot
            {
                GetComponent<PlayerController>().Die();
            }
        }
    }
}
